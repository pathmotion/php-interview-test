<?php

namespace PathMotion;

interface ISemVer
{
    /**
     * Checks if the input string follows the directives of the semver
     * @link https://semver.org/
     */
    public static function isValid(string $version): bool;

    /**
     * Parse string version, and return a valid SemVer instance
     */
    public static function fromString(string $version): SemVer;

    public function __construct(int $major, int $minor = 0, int $patch = 0, string $preReleaseVersion = null, array $buildMetadata = []);

    public function getMajor(): int;
    public function getMinor(): int;
    public function getPatch(): int;

    /**
     * Checks if the current semver instance SHOULD be considered as stable
     * @link https://semver.org/#spec-item-4
     */
    public function isStable(): bool;

    /**
     * set pre release version
     * @link https://semver.org/#spec-item-9
     */ 
    public function setPreReleaseVersion(string $preReleaseVersion = null): self;

    public function hasPreRelease(): bool;

    public function getPreRelease(): ?string;

    /**
     * set build metadata
     * @link https://semver.org/#spec-item-10
     */ 
    public function setBuildMetadata(array $tags = []): self;

    public function hasBuildMetadata(): bool;

    public function getBuildMetadata(): ?string;



    public function isEqual(SemVer $other): bool;
    public function isHigher(SemVer $other): bool;
    public function isLower(SemVer $other): bool;
    public function __toString(): string;
}
