<?php
namespace PathMotion\Tests;

use Exception;
use PathMotion\SemVer;
use PHPUnit\Framework\TestCase;
use stdClass;

class SemVerTest extends TestCase
{

    public function validVersion(): array
    {
        return [
            ['0.0.4'],
            ['1.2.3'],
            ['10.20.30'],
            ['1.1.2-prerelease+meta'],
            ['1.1.2+meta'],
            ['1.1.2+meta-valid'],
            ['1.0.0-alpha'],
            ['1.0.0-beta'],
            ['1.0.0-alpha.beta'],
            ['1.0.0-alpha.beta.1'],
            ['1.0.0-alpha.1'],
            ['1.0.0-alpha0.valid'],
            ['1.0.0-alpha.0valid'],
            ['1.0.0-alpha-a.b-c-somethinglong+build.1-aef.1-its-okay'],
            ['1.0.0-rc.1+build.1'],
            ['2.0.0-rc.1+build.123'],
            ['1.2.3-beta'],
            ['10.2.3-DEV-SNAPSHOT'],
            ['1.2.3-SNAPSHOT-123'],
            ['1.0.0'],
            ['2.0.0'],
            ['1.1.7'],
            ['2.0.0+build.1848'],
            ['2.0.1-alpha.1227'],
            ['1.0.0-alpha+beta'],
            ['1.2.3----RC-SNAPSHOT.12.9.1--.12+788'],
            ['1.2.3----R-S.12.9.1--.12+meta'],
            ['1.2.3----RC-SNAPSHOT.12.9.1--.12'],
            ['1.0.0+0.build.1-rc.10000aaa-kk-0.1'],
            ['1.0.0-0A.is.legal'],
        ];
    }

    /**
     * @dataProvider validVersion
     */
    public function testIsValidMethodWithValidSemVer(string $validVersion): void
    {
        $this->assertTrue(SemVer::isValid($validVersion));
    }

    public function testIsValidMethodWithIValidSemVer(): void
    {
        $this->assertFalse(SemVer::isValid('10.-20.30'));
        $this->assertFalse(SemVer::isValid('1'));
        $this->assertFalse(SemVer::isValid('1.2'));
        $this->assertFalse(SemVer::isValid('1.2.3-0123'));
        $this->assertFalse(SemVer::isValid('1.2.3-0123.0123'));
        $this->assertFalse(SemVer::isValid('1.1.2+.123'));
        $this->assertFalse(SemVer::isValid('+invalid'));
        $this->assertFalse(SemVer::isValid('-invalid'));
        $this->assertFalse(SemVer::isValid('-invalid+invalid'));
        $this->assertFalse(SemVer::isValid('-invalid.01'));
        $this->assertFalse(SemVer::isValid('alpha'));
        $this->assertFalse(SemVer::isValid('alpha.beta'));
        $this->assertFalse(SemVer::isValid('alpha.beta.1'));
        $this->assertFalse(SemVer::isValid('alpha.1'));
        $this->assertFalse(SemVer::isValid('alpha+beta'));
        $this->assertFalse(SemVer::isValid('alpha_beta'));
        $this->assertFalse(SemVer::isValid('alpha.'));
        $this->assertFalse(SemVer::isValid('alpha..'));
        $this->assertFalse(SemVer::isValid('beta'));
        $this->assertFalse(SemVer::isValid('1.0.0-alpha_beta'));
        $this->assertFalse(SemVer::isValid('-alpha.'));
        $this->assertFalse(SemVer::isValid('1.0.0-alpha..'));
        $this->assertFalse(SemVer::isValid('1.0.0-alpha..1'));
        $this->assertFalse(SemVer::isValid('1.0.0-alpha...1'));
        $this->assertFalse(SemVer::isValid('1.0.0-alpha....1'));
        $this->assertFalse(SemVer::isValid('1.0.0-alpha.....1'));
        $this->assertFalse(SemVer::isValid('1.0.0-alpha......1'));
        $this->assertFalse(SemVer::isValid('1.0.0-alpha.......1'));
        $this->assertFalse(SemVer::isValid('01.1.1'));
        $this->assertFalse(SemVer::isValid('1.01.1'));
        $this->assertFalse(SemVer::isValid('1.1.01'));
        $this->assertFalse(SemVer::isValid('1.2'));
        $this->assertFalse(SemVer::isValid('1.2.3.DEV'));
        $this->assertFalse(SemVer::isValid('1.2-SNAPSHOT'));
        $this->assertFalse(SemVer::isValid('1.2.31.2.3----RC-SNAPSHOT.12.09.1--..12+788'));
        $this->assertFalse(SemVer::isValid('1.2-RC-SNAPSHOT'));
        $this->assertFalse(SemVer::isValid('-1.0.3-gamma+b7718'));
        $this->assertFalse(SemVer::isValid('+justmeta'));
        $this->assertFalse(SemVer::isValid('9.8.7+meta+meta'));
        $this->assertFalse(SemVer::isValid('9.8.7-whatever+meta+meta'));
        $this->assertFalse(SemVer::isValid('99999999999999999999999.999999999999999999.99999999999999999----RC-SNAPSHOT.12.09.1--------------------------------..12'));
    }

    public function negativeVersionNumber(): array
    {
        return [
            [-10, 20, 30],
            [10, -20, 30],
            [10, 20, -30],
            [-10],
            [10, -20],
            [-10, -20, 30],
            [-10, -20, -30],
            [10, -20, -30],
            [-10, 20, -30],
        ];
    }

    /**
     * @dataProvider negativeVersionNumber
     */
    public function testsTheInstantiationOfTheObjectWithNegativeVersionNumber(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Version numbers MUST be non-negative integers.');

        new SemVer(-10, 41, 42);
    }

    public function testsTheInstantiationOfTheObjectWithEmptyPreReleaseVersion(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('pre-release version MUST NOT be empty.');

        new SemVer(10, 20, 30, '');
    }

    public function testsTheInstantiationOfTheObjectWithSpecialCharPreReleaseVersion(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('pre-release version MUST include only ASCII alphanumeric characters and hyphens. ([0-9A-Za-z-])');

        new SemVer(10, 20, 30, '*%ALPHA*BETA');
    }

    public function preReleaseVersionNumericIdentifierIncludingLeadingZeroes(): array
    {
        return [
            ['rc.01'],
            ['01']
        ];
    }

    /**
     * @dataProvider preReleaseVersionNumericIdentifierIncludingLeadingZeroes
     */
    public function testsTheInstantiationOfTheObjectWithPreReleaseVersionNumericIdentifierIncludingLeadingZeroes(string $preReleaseVersion): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('pre-release version numeric identifier MUST NOT include leading zeroes');

        new SemVer(10, 20, 30, $preReleaseVersion);
    }

    public function nonStableVersion(): array
    {
        return [
            [0, 1, 42],
            [1, 0, 0, 'alpha'],
        ];
    }

    /**
     * @dataProvider nonStableVersion
     */
    public function testsIsStableWithNonStableVersion(int $major, int $minor, int $patch, string $preReleaseVersion = null): void
    {
        $notStableVersionDueToPreRelease = new SemVer($major, $minor, $patch, $preReleaseVersion);
        $this->assertFalse($notStableVersionDueToPreRelease->isStable());
    }

    public function testsIsStableWithStableVersion(): void
    {
        $stableVersion = new SemVer(1, 0, 42);
        $this->assertTrue($stableVersion->isStable());
    }

    public function testsPreReleaseVersionSetter(): void {
        $version = new SemVer(1, 0, 42, 'alpha.10');
        $this->assertFalse($version->isStable());
        $this->assertTrue($version->hasPreRelease());
        $this->assertEquals($version->getPreRelease(), 'alpha.10');

        $version->setPreReleaseVersion(null);
        $this->assertTrue($version->isStable());
        $this->assertFalse($version->hasPreRelease());
        $this->assertEquals($version->getPreRelease(), null);

        $version->setPreReleaseVersion('beta');
        $this->assertFalse($version->isStable());
        $this->assertTrue($version->hasPreRelease());
        $this->assertEquals($version->getPreRelease(), 'beta');
    }

    public function testsBuildMetadataSetterWithInvalidTagType(): void {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Build metadata should an array of strings or integers');

        new SemVer(1, 0, 42, null, [['hello'], new stdClass(), 10.10]);
    }

    public function testsBuildMetadataSetterWithEmptyTag(): void {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Build metadata MUST NOT be empty');

        new SemVer(1, 0, 42, null, ['hello', '', 'world']);
    }

    public function testsBuildMetadataSetterWithInvalidFormatTag(): void {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('build metadata MUST include only ASCII alphanumeric characters and hyphens. ([0-9A-Za-z-])');

        new SemVer(1, 0, 42, null, ['hello.world-10']);
    }

    public function testsBuildMetadataSetterWithValidFormatTag(): void {
        $version = new SemVer(1, 0, 42, null, ['ab', 'lab-orum']);
        $this->assertTrue($version->hasBuildMetadata());
        $this->assertEquals($version->getBuildMetadata(), 'ab.lab-orum');

        $version->setBuildMetadata([]);
        $this->assertFalse($version->hasBuildMetadata());
        $this->assertEquals($version->getBuildMetadata(), null);

        $version->setBuildMetadata(['1626959106548']);
        $this->assertTrue($version->hasBuildMetadata());
        $this->assertEquals($version->getBuildMetadata(), '1626959106548');
    }

    public function toStringProvider(): array
    {
        return [
            [9, 84, 0, null, [], '9.84.0'],
            [7, 34, 16, 'alpha', [], '7.34.16-alpha'],
            [3, 6110, 1, 'alpha', [10], '3.6110.1-alpha+10'],
            [4, 22, 42, 'alpha', [10, 'inventore'], '4.22.42-alpha+10.inventore'],
            [8, 30, 5, null, [10, 'inventore'], '8.30.5+10.inventore'],
        ];
    }

    /**
     * @dataProvider toStringProvider
     */
    public function testToString(
        int $major,
        int $minor = null,
        int $patch = null,
        string $preReleaseVersion = null,
        array $buildMetadata = [],
        string $expectedOutput
    ) {
        $version = new SemVer($major, $minor, $patch, $preReleaseVersion, $buildMetadata);

        $this->assertEquals((string)$version, $expectedOutput);
    }

    public function isEqualProvider(): array
    {
        return [
            [
                new SemVer(0, 1, 2, null, ['10', '20']),
                new SemVer(0, 1, 2, null, ['10', '20']),
                true
            ],
            [
                new SemVer(1, 1, 2, null, ['10', '20']),
                new SemVer(2, 1, 2, null, ['10', '20']),
                false
            ],
            [
                new SemVer(1, 1, 2, 'a', ['10', '20']),
                new SemVer(1, 1, 2, 'b', ['10', '20']),
                false
            ],
            [
                new SemVer(1, 1, 2, 'a', ['10', '20']),
                new SemVer(1, 1, 2, 'a', []),
                true
            ],
        ];
    }

    /**
     * @dataProvider isEqualProvider
     */
    public function testIsEqual(SemVer $left, SemVer $right, bool $expected) {
        $this->assertEquals($left->isEqual($right), $expected);
    }

    public function isHigherProvider(): array
    {
        return [
            [
                new SemVer(0, 1, 2),
                new SemVer(1, 1, 2),
                true
            ],
            [
                new SemVer(1, 1, 2),
                new SemVer(0, 1, 2),
                false
            ],
            [
                new SemVer(1, 1, 2),
                new SemVer(1, 1, 2),
                false
            ],
            [
                new SemVer(1, 1, 2),
                new SemVer(1, 2, 2),
                true
            ],
            [
                new SemVer(1, 2, 2),
                new SemVer(1, 2, 3),
                true
            ],
            [
                new SemVer(1, 2, 3),
                new SemVer(1, 2, 2),
                false
            ],
            [
                new SemVer(1, 2, 3, 'hello'),
                new SemVer(1, 2, 3),
                true
            ],
            [
                new SemVer(1, 2, 3),
                new SemVer(1, 2, 3, 'hello'),
                false
            ],
            [
                new SemVer(1, 2, 3, 'hello'),
                new SemVer(1, 2, 3, 'hello'),
                false
            ],
            [
                new SemVer(1, 2, 3, 'hello', [10]),
                new SemVer(1, 2, 3, 'hello', [11]),
                false
            ],
        ];
    }

    /**
     * @dataProvider isHigherProvider
     */
    public function testIsHigher(SemVer $left, SemVer $right, bool $expected) {
        $this->assertEquals($left->isHigher($right), $expected);
    }


    public function isLowerProvider(): array
    {
        return [
            [
                new SemVer(0, 1, 2),
                new SemVer(1, 1, 2),
                false
            ],
            [
                new SemVer(1, 1, 2),
                new SemVer(0, 1, 2),
                true
            ],
            [
                new SemVer(1, 1, 2),
                new SemVer(1, 1, 2),
                false
            ],
            [
                new SemVer(1, 1, 2),
                new SemVer(1, 2, 2),
                false
            ],
            [
                new SemVer(1, 2, 2),
                new SemVer(1, 2, 3),
                false
            ],
            [
                new SemVer(1, 2, 3),
                new SemVer(1, 2, 2),
                true
            ],
            [
                new SemVer(1, 2, 3, 'hello'),
                new SemVer(1, 2, 3),
                false
            ],
            [
                new SemVer(1, 2, 3),
                new SemVer(1, 2, 3, 'hello'),
                true
            ],
            [
                new SemVer(1, 2, 3, 'hello'),
                new SemVer(1, 2, 3, 'hello'),
                false
            ],
            [
                new SemVer(1, 2, 3, 'hello', [10]),
                new SemVer(1, 2, 3, 'hello', [11]),
                false
            ],
        ];
    }

    /**
     * @dataProvider isLowerProvider
     */
    public function testIsLower(SemVer $left, SemVer $right, bool $expected) {
        $this->assertEquals($left->isLower($right), $expected);
    }

    /**
     * @dataProvider validVersion
     */
    public function testFromString(string $validVersion)
    {
        $valid = SemVer::fromString($validVersion);

        $this->assertInstanceOf(SemVer::class, $valid);
        $this->assertEquals($validVersion, (string)$valid);
    }
}