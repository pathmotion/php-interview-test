# PHP interview test

This is a test used to assess technical skills for PathMotion developer recruitment.

## Installation

Use [composer](https://getcomposer.org/) to install dependencies.

```bash
composer install
```

## Instruction
To perform this test you will need to implement the `PathMotion\SemVer` class following the `PathMotion\ISemVer` interface.

This class represents the implementation of Semantic Management version 2.0.0 ([semver](https://semver.org))

To help you with this task, feel free to use the official ([semver](https://semver.org)) specifications and unit tests that have already been written for you.
The mission is considered successful if all unit tests are green, of course you do not have to modify/delete existing tests...

## Running UnitTest
```bash
docker run --rm -v $(pwd):/app jitesoft/phpunit
```

## Bonus
- Extends the coverage of your code to `100%`.
